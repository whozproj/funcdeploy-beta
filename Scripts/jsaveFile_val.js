﻿$(document).ready(function () {
    $("input#namefile").keyup(function () {
        $("label#command").html(($(this).val().length > 0) ? $(this).val() : "Sin Titulo");
        checkAllFunctions();
    });
    $("input#opt1").change(function () {
        checkAllFunctions();
    });
    $("input#opt2").change(function () {
        checkAllFunctions();
    });
    $("input#opt3").change(function () {
        checkAllFunctions();
    });
    $("input#enableText").change(function () {
        if($(this).is(":checked")){
            $("textarea#obText").removeAttr("disabled");
        } else {
            $("textarea#obText").attr("disabled", "disabled");
            $("textarea#obText").val(null);
        }
        checkAllFunctions();
    });
    $("textarea#obText").keyup(function () {
        checkAllFunctions();
    });
    function checkAllFunctions() {
        var check = false;
        var message = "INPUT";
        $("p#msgalert").css("display", "none");
        if ($("input#opt1").is(":checked") || $("input#opt2").is(":checked") || $("input#opt3").is(":checked")) {
            if ($("input#namefile").val().match(/^[a-zA-Z0-9--_]+$/)) {
                check = true;
            } else {
                $("p#msgalert").css("display", "inherit");
                message = "El nombre del archivo no puede tener caracteres ni espacios de tipo: '! \" $ # % & / ( ) = ? ¿ ¡ @'";
                check = false;
            }
            if ($("input#enableText").is(":checked")) {
                if ($("textarea#obText").val().length > 0 && $("textarea#obText").val().match(/^[a-zA-Z0-9\s]+$/)) {
                    check = true;
                } else {
                    $("p#msgalert").css("display", "inherit");
                    if (!$("textarea#obText").val().match(/^[a-zA-Z0-9\s]+$/)) {
                        message = "Hay caracteres no validos en el campo de observacion";
                    } else {
                        message = "Ingresa una observacion para el documento";
                    }
                    check = false;
                }
            }
        } else {
            $("p#msgalert").css("display", "inherit");
            message = "Debes seleccionar al menos un elemento para guardar";
            check = false;
        }
        $("p#msgalert").html(message);
        finalCheck(check);
    }
    function finalCheck(checking) {
        var opt = checking;
        if (opt) {
            $("button#savestate").removeAttr("disabled");
        } else {
            $("button#savestate").attr("disabled","disabled");
        }
    }
    $("#formatfile").change(function () {
        var conceptName = $(this).find(":selected").val();
        if(conceptName=="true"){
            $("img#imafile").attr("src", "/SubITS/Image/xls.png");
            $("p#msgalert").css("display", "inherit");
            $("p#msgalert").html("Estimado, esta a punto de guardar en un formato editable, por su seguridad este documento se guardara con la directiva de contraseñas de uso personal el cual necesitara usar su contraseña de acceso para abrir este archivo");
        } else {
            $("img#imafile").attr("src", "/SubITS/Image/pdf.png");
            $("p#msgalert").css("display", "none");
        }
    });
    $("button#stopstate").click(function () {
        $("input#namefile").val(null);
        $("input#enableText").removeAttr("checked");
        $("textarea#obText").val(null);
        $("textarea#obText").attr("disabled", "disabled");
        $("input#opt1").removeAttr("checked");
        $("input#opt2").removeAttr("checked");
        $("input#opt3").removeAttr("checked");
        $("button#savestate").attr("disabled", "disabled");
    });
});