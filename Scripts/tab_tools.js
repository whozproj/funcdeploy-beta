﻿$(document).ready(function() {
    $("#loaderSystem").css("display", "none");
    $("#critical").change(function () {
        if($(this).is(":checked")){
            $("#sendalert").removeAttr("disabled");
        } else {
            $("#sendalert").attr("disabled","disabled");
        }
    });
    $("#sendalert").change(function () {
        $("#customID").css("display",$(this).is(":checked")?"inherit":"none");
    });
});