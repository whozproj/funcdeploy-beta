﻿$(document).ready(function () {
    $("input#usermod").keyup(function () {
        $("tr#getdisplay").css("display", "none");
        $("img#useralert").css("display", "none");
        var status = false;
        var msg = null;
        if ($(this).val().match(/^[a-zA-Z0-9--_]+$/)) {
            status = true;
        } else {
            $("img#useralert").css("display", "inherit");
            $("tr#getdisplay").css("display", "inherit");
            msg = "No puedes utilizar ningun caracter especial en el nombre de usuario que quieres crear, por favor evite usar estas siguientes caracteres: \",',?,¿,¡,!,@,$,~,<,>,#,·,%,&,/,\\,(,),€";
            status = false;
        }
        $("p#inside").html(msg);
        systemValidator(status);
    });
    $("input#passmodifier").change(function () {
        if ($(this).is(":checked")) {
            $("input#passmod").css("display", "inherit");
        } else {
            $("input#passmod").css("display", "none");
            $("input#passmod").val(null);
        }
        systemValidator(false);
    });
    $("input#passmod").keyup(function () {
        $("tr#getdisplay").css("display", "none");
        $("img#useralert").css("display", "none");
        if ($("input#passmodifier").is(":checked")) {
            var status = false;
            var msg = null;
            if ($(this).val().match(/.{5,}/) && $("input#passmod").val().match(/^[a-zA-Z0-9--_:-?$-@]+$/)) {
                $("img#pswalert").css("display", "none");
                status = true;
            } else {
                $("img#pswalert").css("display", "inherit");
                $("tr#getdisplay").css("display", "inherit");
                msg = "Su contraseña NO debe tener '\"COMILLAS\"', solo simbolos o numeros para reforzarla y debe tener como minimo 5 caracteres";
                status = false;
            }
            $("p#inside").html(msg);
            systemValidator(status);
        } else {
            $("img#pswalert").css("display", "none");
        }
    });
    $("input#names").keyup(function () {
        $("tr#getdisplay").css("display", "none");
        $("img#useralert").css("display", "none");
        var status = false;
        var msg = null;
        if ($(this).val().match(/^[a-zA-Z0-9--_\s]+$/)) {
            $("img#namealert").css("display", "none");
            status = true;
        } else {
            $("tr#getdisplay").css("display", "inherit");
            msg = "Por favor no uilize caracteres especiales en el nombre";
            $("img#namealert").css("display", "inherit");
            status = false;
        }
        $("p#inside").html(msg);
        systemValidator(status);
    });
    $("input#ape").keyup(function () {
        $("tr#getdisplay").css("display", "none");
        $("img#useralert").css("display", "none");
        if ($(this).val().length > 0) {
            var status = false;
            var msg = null;
            if ($(this).val().match(/^[a-zA-Z0-9--_\s]+$/)) {
                $("img#apellidoalert").css("display", "none");
                status = true;
            } else {
                $("tr#getdisplay").css("display", "inherit");
                msg = "Por favor no uilize caracteres especiales en el apellido, si no quiere escribir su apellido por favor dejela en blanco";
                $("img#apellidoalert").css("display", "inherit");
                status = false;
            }
            $("p#inside").html(msg);
            systemValidator(status);
        }
    });
    $("input#phonemod").keyup(function () {
        $("tr#getdisplay").css("display", "none");
        $("img#useralert").css("display", "none");
        var status = false;
        var msg = null;
        if ($(this).val().match(/.{8,}/) && $("input#phonemod").val().match(/^[0-9]+$/)) {
            $("img#phonealert").css("display", "none");
            status = true;
        } else {
            $("tr#getdisplay").css("display", "inherit");
            msg = "Debe especificar su numero telefono o movil correspondiente";
            $("img#phonealert").css("display", "inherit");
            status = false;
        }
        $("p#inside").html(msg);
        systemValidator(status);
    });
    $("input#emailmod").keyup(function () {
        $("tr#getdisplay").css("display", "none");
        $("img#useralert").css("display", "none");
        var status = false;
        var msg = null;
        if ($(this).val().match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) {
            $("img#emailalert").css("display", "none");
            status = true;
        } else {
            $("tr#getdisplay").css("display", "inherit");
            msg = "Debe especificar un correo electronico personal para la persona el cual sera enviada al representante: ejemplo@domain.cl";
            $("img#emailalert").css("display", "inherit");
            status = false;
        }
        $("p#inside").html(msg);
        systemValidator(status);
    });
    function systemValidator(sys) {
        var status = sys;
        
    }
});